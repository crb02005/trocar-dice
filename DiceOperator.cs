﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trocar.Dice
{
    public enum DiceOperator
    {
        Add,
        Subtract,
        Multiply,
        Divide
    }
}
