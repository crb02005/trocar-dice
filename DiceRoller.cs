﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trocar.Dice
{
    public class DiceRoller
    {
        private readonly ILogger<DiceRoller> logger;
        private readonly Random random;
        private readonly Dictionary<DiceOperator, Func<int, int, int>> lookup;

        public DiceRoller(ILogger<DiceRoller> logger, Random random)
        {
            logger.LogTrace("Initializing the DiceRoller");
            this.logger = logger;
            this.random = random;
            this.lookup = new Dictionary<DiceOperator, Func<int, int, int>> {
                {
                    DiceOperator.Add,
                    (x,y)=>x+y
                },
                {
                    DiceOperator.Subtract,
                    (x,y)=>x-y
                },
                {
                    DiceOperator.Multiply,
                    (x,y)=>x*y
                },
                {
                    DiceOperator.Divide,
                    (x,y)=>x/y
                }
            };
        }
        public int Roll(DiceExpression diceExpression)
        {
            int[] results = Enumerable.Range(1, diceExpression.Count).Select((x) => random.Next(1,diceExpression.AmountOfSides+1)).ToArray();
            logger.LogTrace("Dice Rolls:{0}", string.Join(',', results));
            int result = lookup[diceExpression.Operator](results.Sum(), diceExpression.Operand);
            logger.LogTrace("Dice Result With Modifiers:{0}", result);
            return result;
        }
            
    }
}
