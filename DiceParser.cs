﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Trocar.Dice
{
    public class DiceParserOptions
    {
        public Dictionary<string,string> NamedDice { get; set; }
    }
    public enum DiceParseState
    {
        Count,
        Amount,
        Operand,
        Operator
    }


    public class DiceParser
    {
        public DiceParser(IOptions<DiceParserOptions> options)
        {
            _options = options.Value;
        }

        const string mathematical = "+-/*";
        private readonly DiceParserOptions _options;

        public DiceExpression Parse(string s)
        {
            if (_options.NamedDice.ContainsKey(s))
            {
                return Parse(_options.NamedDice[s]);
            }

            DiceParseState parseState = DiceParseState.Count;
            string rawAmount = "";
            string rawCount = "";
            string rawOperand = "";
            DiceOperator diceOperator = DiceOperator.Add;
            Dictionary<char, DiceOperator> OperatorLookup = new Dictionary<char, DiceOperator> {
                { '+',DiceOperator.Add},
                { '-',DiceOperator.Subtract},
                { '/',DiceOperator.Divide},
                { '*',DiceOperator.Multiply}
            };
            Dictionary<DiceParseState, Action<char>> p = null;
            p = new Dictionary<DiceParseState, Action<char>>
            {
                { DiceParseState.Count,(c)=>{
                    if (c == 'd')
                    {
                        parseState+=1;
                    } else if(Char.IsDigit(c))
                    {
                        rawCount+=c;
                    } else
                    {
                        throw new ArgumentException("Count must be Digit");
                    }
                }},
                {
                    DiceParseState.Amount,
                    (c) =>
                    {
                     if(mathematical.IndexOf(c)>-1){
                        p[DiceParseState.Operator](c);
                     } else if(Char.IsDigit(c))
                        {
                            rawAmount+=c;
                        } else
                        {
                            throw new ArgumentException("Amount of sides must be Digit");
                        }

                    }
                },
                {
                    DiceParseState.Operator,
                    (c) =>
                    {
                       diceOperator =OperatorLookup[c];
                       parseState =DiceParseState.Operand;
                    }
                },
                {
                    DiceParseState.Operand,
                    (c) =>
                    {
                        if (Char.IsDigit(c))
                        {
                            rawOperand+=c;
                        } else
                        {
                            throw new ArgumentException("Operand must be a number.");
                        }
                    }
                }
            };
            foreach (char c in s)
            {
                p[parseState](c);
            }
            return new DiceExpression()
            {
                AmountOfSides = int.Parse(rawAmount),
                Count = int.Parse(rawCount),
                Operand = rawOperand.Length > 0? int.Parse(rawOperand):0,
                Operator = diceOperator
            };
        }

        public DiceExpression Parse(string[] pieces) => Parse(pieces[0]);

    }
}
