# trocar-dice

A console dice roller

## General Usage:

### Roll one twenty sided die.

```
Trocar.Dice.exe 1d20
```


### Roll five six sided dice and add one.

```
Trocar.Dice.exe 5d6+1
```

Possible math operations: +-/*

### Named dice expressions

See appSettings.json
Example Scythe:

```
Trocar.Dice.exe Scythe
```


