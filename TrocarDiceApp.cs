﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Trocar.Dice
{
    public class TrocarDiceApp: Trocar.ConsoleHarness.TrocarApplication
    {
        private string[] _args;
        private const string USAGE = "TrocarDiceApp <numberOfDice>d<countOfSides>(+-/*)<modifier>";
        protected override void onConfigureServices(string[] args, IServiceCollection sc, IConfiguration configuration)
        {
            base.onConfigureServices(args, sc,configuration);
            _args = args;
            var diceParserSection = configuration.GetSection("DiceParserOptions");
            sc.Configure<DiceParserOptions>(diceParserSection);
            sc.AddSingleton<Random, Random>();
            sc.AddSingleton<DiceParser, DiceParser>();
            sc.AddSingleton<DiceRoller, DiceRoller>();
        }
        protected override void onStart(ServiceProvider sp)
        {
            base.onStart(sp);
            DiceParser dp =sp.GetService<DiceParser>();
            DiceRoller dr= sp.GetService<DiceRoller>();
            if (_args.Length > 0)
            {
                if (_args[0] == "--i")
                {
                    Console.WriteLine("REPL: x to quit");
                    string l = Console.ReadLine();
                    DiceExpression diceExpression;
                    while (l != "x")
                    {
                        try
                        {
                            diceExpression = dp.Parse(l.Split("\n"));
                            Console.WriteLine($"Rolled {diceExpression.Count}d{diceExpression.AmountOfSides} {diceExpression.Operator.ToString()} {diceExpression.Operand}: { dr.Roll(diceExpression)}");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        l = Console.ReadLine();
                    }
                } else
                {
                    DiceExpression diceExpression = dp.Parse(_args);
                    Console.WriteLine($"Rolled {diceExpression.Count}d{diceExpression.AmountOfSides} {diceExpression.Operator.ToString()} {diceExpression.Operand}: { dr.Roll(diceExpression)}");
                }
            } else
            {
                Console.Write(USAGE);
            }
            
        }
    }
}
