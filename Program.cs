﻿using System;
using Trocar.ConsoleHarness;

namespace Trocar.Dice
{
    class Program : Trocar.ConsoleHarness.TrocarProgram<TrocarDiceApp>
    {
        static void Main(string[] args)
        {
            TrocarProgram<TrocarDiceApp>.Main(args);
        }
    }
}
