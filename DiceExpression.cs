﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trocar.Dice
{
    public class DiceExpression
    {
        public int AmountOfSides { get; set; }
        public int Count { get; set; }
        public DiceOperator Operator { get; set; }
        public int Operand { get; set; }
    }
}
